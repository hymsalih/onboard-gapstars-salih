<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OnboardController;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/login', [AdminController::class, 'index'])->name('login');
Route::get('/logout', [AdminController::class, 'logout'])->name('logout');
Route::post('/admin/login', [AdminController::class, 'doLogin'])->name('admin-login');
Route::get('/on-board', [OnboardController::class, 'index'])->name('on-board');
