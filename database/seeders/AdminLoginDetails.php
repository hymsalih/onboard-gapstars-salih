<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Models\User;
use Hash;

class AdminLoginDetails extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = ['id' => 1, 'name' => 'admin', 'email' => 'admin@test.com', 'password' => Hash::make('pwd@1')];
        if (!User::find($user['id'])) {
            User::insert($user);
        }
    }
}
