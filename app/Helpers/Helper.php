<?php

namespace App\Helpers;

use Carbon\Carbon;
use Storage;

class Helper
{
    public static function csvToArray()
    {
        $fileName = Storage::path('chart-data/export.csv');
        if (!file_exists($fileName))
            die("Sorry Chart source data not found. Please upload the file under storage/app/chart-data/export.csv");

        if (!file_exists($fileName) || !is_readable($fileName))
            return false;

        $header = NULL;
        $csvData = array();
        if (($handle = fopen($fileName, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if (!$header)
                    $header = $row;
                else
                    $csvData[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        $dataGroupByWeek = [];
        foreach ($csvData as $data) {
            $weekDate = Helper::getWeekDates($data['created_at']);
            $dataGroupByWeek[$weekDate['week_start']][] = $data['onboarding_perentage'];
        }
        foreach ($dataGroupByWeek as $date => $item) {
            $dataGroupByWeek[$date] = $item;
        }
        return $dataGroupByWeek;
    }

    public static function getWeekDates($createdAt)
    {
        $week = Carbon::createFromFormat('Y-m-d', $createdAt);
        return ['week_start' => $week->startOfWeek()->format('Y-m-d'), 'week_end' => $week->endOfWeek()->format('Y-m-d')];
    }

}
