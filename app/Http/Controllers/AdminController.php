<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.admin-login');
    }

    public function doLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return redirect()->route('on-board');
        }
        return redirect()->route("login")->withSuccess('Login details are not valid');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route("login");
    }
}
