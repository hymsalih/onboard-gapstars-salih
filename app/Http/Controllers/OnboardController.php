<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Helper;
use \App\API\ChartAPI;

class OnboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $csvData = Helper::csvToArray();
        $chartAPI = new ChartAPI($csvData);
        $chartData = $chartAPI->chartData();
        return view('admin.on-board.index', compact('chartData'));
    }
}
