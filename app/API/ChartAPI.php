<?php


namespace App\API;

use Carbon\Carbon;
use Illuminate\Http\Client\Request;

class ChartAPI
{
    private $chartData;

    public function __construct($dataGroupByWeek)
    {
        $this->chartData = $dataGroupByWeek;
    }

    public function chartData()
    {
        $dataGroupByWeek = $this->chartData;
        $chartArray = [];
        $chartArray["chart"] = array(
            "type" => "line"
        );
        $chartArray["title"] = array(
            "text" => "Weekly Retention Curve"
        );
        $chartArray["credits"] = array(
            "enabled" => true
        );
        $chartArray ["xAxis"] = array(
            "categories" => array()
        );
        $chartArray ["tooltip"] = array(
            "valueSuffix" => "%"
        );
        $categoryArray = array(
            '0',
            '20',
            '40',
            '50',
            '70',
            '90',
            '99',
            '100'
        );
        $chartArray ["xAxis"] = array(
            "categories" => $categoryArray
        );
        $chartArray ["yAxis"] = array(
            "title" => array(
                "text" => "Total Onboarded"
            ),
            'labels' => array(
                'format' => '{value}%'
            ),
            'min' => '0',
            'max' => '100'
        );
        foreach ($dataGroupByWeek as $key => $week) {
            $percentage_0 = $percentage_20 = $percentage_40 = $percentage_50 = $percentage_70 = $percentage_90 = $percentage_99 = $percentage_100 = 0;
            $total_sum = 0;
            foreach ($week as $percentage) {
                if (empty($percentage))
                    continue;
                if ($percentage < 20) {
                    $percentage_0 += $percentage;
                } else if ($percentage >= 20 && $percentage < 40) {
                    $percentage_20 += $percentage;
                } else if ($percentage >= 40 && $percentage < 50) {
                    $percentage_40 += $percentage;
                } else if ($percentage >= 50 && $percentage < 70) {
                    $percentage_50 += $percentage;
                } else if ($percentage >= 70 && $percentage < 90) {
                    $percentage_70 += $percentage;
                } else if ($percentage >= 90 && $percentage < 99) {
                    $percentage_90 += $percentage;
                } else if ($percentage >= 99 && $percentage < 100) {
                    $percentage_99 += $percentage;
                } else if ($percentage >= 100) {
                    $percentage_100 += $percentage;
                }
                $total_sum += $percentage;
            }
            $chartArray ["series"] [] = array(
                "name" => $key,
                "data" => [
                    round(($percentage_0 / $total_sum) * 100),
                    round(($percentage_20 / $total_sum) * 100),
                    round(($percentage_40 / $total_sum) * 100),
                    round(($percentage_50 / $total_sum) * 100),
                    round(($percentage_70 / $total_sum) * 100),
                    round(($percentage_90 / $total_sum) * 100),
                    round(($percentage_99 / $total_sum) * 100),
                    round(($percentage_100 / $total_sum) * 100),
                ]
            );
        }
        return response()->json($chartArray);
    }
}
