<?php

namespace Tests\Feature;

use App\API\ChartAPI;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Helpers\Helper;
use Storage;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCsvToArrayEmptyFileName()
    {
        $csv = Helper::csvToArray(":", "");
        $this->assertFalse($csv);
    }
}
