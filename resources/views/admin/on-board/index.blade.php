<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <title>Weekly retention curve chart</title>
</head>
<body>
<h1>Weekly retention curve chart</h1>
<a href="{{route('logout')}}" class="btn btn-info btn-lg">
    Log out
</a>
<div id="container"></div>
</body>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript">
    Highcharts.chart('container',{!! $chartData->getContent() !!} );
</script>
</html>
