
## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## On Board Application - Admin
After login as Admin, Admin can view the progress of the user's enrollment through the highchart

## Setup

NOTE This application developed using Laravel 8.54 and above and It's required 7.3 or 8.0. So skip these Setup instructions if you're using Laravel 8.54 and above.

### Installation

Clone the application from repository to your local matchine
```sh
git clone https://hymsalih@bitbucket.org/hymsalih/onboard-gapstars-salih.git
```

```sh
cd onboard-gapstars-salih
```

Install laravel package lists
```sh
composer install
```


### Configuration
Create environment file, rename .env.example to.env file and add your database connection

```sh
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=DB_NAME
DB_USERNAME=DB_USER_NAME
DB_PASSWORD=DB_PASSWORD
```

NOTE:- Check your local database connections. if it's not connected please turn on your local matchine databse engine.

Genearte laravel key
```sh
php artisan key:generate
```


Run artisan command to load defult tables

```sh
php artisan migrate
```
After that create default admin user creadentials using laravel db:seeder
Open the project console prompt and type below artisan command and hit enter

```sh
php artisan db:seed --class=AdminLoginDetails
```
### Upload Data source (export.csv) to storage folder

Note:- Please upload the file under storage/app/chart-data/export.csv and run below command to publish the laravel storage link

```sh
php artisan storage:link
```

Once the above processes are successfully done start-server to run the application 

```sh
php artisan serve
```

### Application routes and User Credential
Admin login route - http://127.0.0.1:8000/login
default user name and password
```sh
admin@test.com
pwd@1
```

#### Application on board chart image link
```sh
http://app.milehighairguns.shop/public/on-board-chart.png
```
#### Thank You!